﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LG.Demo.Dto;

namespace LG.Demo.Service.Controllers
{
    [ApiController]
    [Area("app")]
    [Route("api/app/brand")]
    public class BrandController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<BrandDto> Get()
        {
            return new List<BrandDto>
            {
                new BrandDto("14532", "Lassa"),
                new BrandDto("223SX", "Michelin"),
                new BrandDto("1123", "Petlas"),
                new BrandDto("83E1C982-8617-498F-8988-6D277B04AA94", "Bridgestone"),
                new BrandDto("ESDFTGCS", "Kumho")
            };
        }
    }
}
