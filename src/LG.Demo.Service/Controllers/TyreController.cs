﻿using System.Collections.Generic;
using System.Linq;
using LG.Demo.Dto;
using Microsoft.AspNetCore.Mvc;

namespace LG.Demo.Service.Controllers
{
    [ApiController]
    [Area("app")]
    [Route("api/app/tyre")]
    public class TyreController : ControllerBase
    {

        private readonly LastikGonderMocker _lastikGonderMocker;

        public TyreController()
        {
            _lastikGonderMocker = new LastikGonderMocker();
        }

        [HttpPost]
        public IEnumerable<ApiPortalResponseDto> Post([FromBody] ApiPortalRequestDto requestDto)
        {
            return _lastikGonderMocker.FakeDatas.Where((ApiPortalResponseDto l) => l.ProductName.Contains(requestDto.Text));
        }
    }
}