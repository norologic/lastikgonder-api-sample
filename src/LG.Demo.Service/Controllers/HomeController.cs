﻿using Microsoft.AspNetCore.Mvc;

namespace LG.Demo.Service.Controllers
{
    public class HomeController : ControllerBase
    {
        public ActionResult Index()
        {
            return Redirect("~/swagger");
        }
    }
}