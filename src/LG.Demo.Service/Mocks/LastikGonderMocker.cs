﻿using System.Collections.Generic;
using System.Linq;
using Bogus;
using Bogus.DataSets;
using LG.Demo.Dto;

namespace LG.Demo.Service.Controllers
{
    public class LastikGonderMocker
    {
        private List<ApiPortalResponseDto> _apiPortals = new List<ApiPortalResponseDto>();

        public IEnumerable<ApiPortalResponseDto> FakeDatas => _apiPortals;

        public LastikGonderMocker()
        {
            var tyres = new string[] { "1956515", "2055516", "2055517" };
            var brands = new string[] { "Lassa", "Bridgestone", "Michelin", "Petlas", "Kormoran" };
            var models = new string[] { "Greenways", "Snoways", "Blizzak", "LM001", "EfficientGrip" };
            var speedIndex = new string[] { "91V", "82H", "91H", "102T", "94T" };
            Address address = new Address("tr");

            var stockGenerator = new Faker<ApiPortalResponseStockDto>().StrictMode(true)
                .RuleFor(t => t.Amount, f => f.Random.Number(1, 10).ToString())
                .RuleFor(t => t.Store, f => address.City());

            var responseGenerator = new Faker<ApiPortalResponseDto>().StrictMode(false)
                .RuleFor(t => t.ProductName, f => f.PickRandom(tyres) + " " + f.PickRandom(models) + " " + f.PickRandom(speedIndex))
                .RuleFor(t => t.Stocks, f => stockGenerator.Generate(2).ToList())
                .RuleFor(t => t.ProductId, f => f.UniqueIndex.ToString())
                .RuleFor(t => t.Brand, f => f.PickRandom(brands))
                .RuleFor(t => t.TaxIncludedPrice, f => f.Finance.Amount())
                .RuleFor(t => t.Season, f => ((int)f.PickRandom<Seasons>()).ToString())
                .RuleFor(t => t.Category, f => ((int)f.PickRandom<Categories>()).ToString());

            _apiPortals.AddRange(responseGenerator.Generate(100));
        }
    }
}