﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LG.Demo.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LG.Demo.ConsoleClient
{
    class Program
    {
		private const string _apiUrl = "https://localhost:5001/api/app/tyre";

        private static async Task Main(string[] args)
        {
            Console.WriteLine("Starting API Search");
            await StartApiSearch();
            Console.WriteLine("Press Any Key For Application Shutdown");
            Console.ReadLine();
        }

        private static async Task StartApiSearch()
        {
            ApiPortalRequestDto portalRequestDto = new ApiPortalRequestDto("1956515");
            Console.WriteLine("Searching " + portalRequestDto.Text);
            Console.WriteLine($"Results Count: {(await CallApi(portalRequestDto)).Count}");
        }

        private static async Task<List<ApiPortalResponseDto>> CallApi(ApiPortalRequestDto portalRequest)
        {
            using var client = new HttpClient();
            client.BaseAddress = new Uri(_apiUrl);
            var content = new StringContent(
                JsonConvert.SerializeObject(portalRequest),
                Encoding.UTF8,
                "application/json"
            );

            var httpResponse = await client.PostAsync("", content);
            if (httpResponse.IsSuccessStatusCode)
            {
                var response = await httpResponse.Content.ReadAsStringAsync();
                Console.WriteLine(JToken.Parse(response).ToString());
                return JsonConvert.DeserializeObject<List<ApiPortalResponseDto>>(response);
            }

            throw new HttpRequestException(httpResponse.RequestMessage.ToString());
        }
	}
}
