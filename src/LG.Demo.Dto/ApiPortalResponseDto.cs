﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace LG.Demo.Dto
{
    public class ApiPortalResponseDto
    {
        [JsonProperty("productId")]
        public string ProductId
        {
            get;
            set;
        }

        [JsonProperty("productName")]
        public string ProductName
        {
            get;
            set;
        }

        [JsonProperty("brand")]
        public string Brand
        {
            get;
            set;
        }

        [JsonProperty("category")]
        public string Category
        {
            get;
            set;
        }

        [JsonProperty("season")]
        public string Season
        {
            get;
            set;
        }

        [JsonProperty("taxIncludedPrice")]
        public decimal TaxIncludedPrice
        {
            get;
            set;
        }

        [JsonProperty("taxExcludedPrice")]
        public decimal TaxExcludedPrice
        {
            get;
            set;
        }

        [JsonProperty("discountedPrice")]
        public decimal DiscountedPrice
        {
            get;
            set;
        }

        [JsonProperty("speedLoadIndex")]
        public string SpeedLoadIndex
        {
            get;
            set;
        }

        [JsonProperty("dotCode")]
        public string DotCode
        {
            get;
            set;
        }

        [JsonProperty("speedIndex")]
        public string SpeedIndex
        {
            get;
            set;
        }

        [JsonProperty("loadIndex")]
        public string LoadIndex
        {
            get;
            set;
        }

        [JsonProperty("extraLoad")]
        public bool ExtraLoad
        {
            get;
            set;
        }

        [JsonProperty("taxRate")]
        public decimal TaxRate
        {
            get;
            set;
        }

        [JsonProperty("labels")]
        public ApiPortalResponseLabelDto Label
        {
            get;
            set;
        }

        [JsonProperty("stocks")]
        public List<ApiPortalResponseStockDto> Stocks
        {
            get;
            set;
        }
    }
}