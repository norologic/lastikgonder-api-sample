﻿using System;

namespace LG.Demo.Dto
{
    public class ApiPortalRequestDto
    {
        public string Text
        {
            get;
            set;
        }

        public ApiPortalRequestDto(string text)
        {
            Text = text;
        }
    }
}
