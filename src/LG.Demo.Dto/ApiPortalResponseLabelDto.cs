﻿using Newtonsoft.Json;

namespace LG.Demo.Dto
{
    public class ApiPortalResponseLabelDto
    {
        [JsonProperty("loud")]
        public string Loud
        {
            get;
            set;
        }

        [JsonProperty("gas")]
        public string Gas
        {
            get;
            set;
        }

        [JsonProperty("wet")]
        public string Wet
        {
            get;
            set;
        }
    }
}