﻿namespace LG.Demo.Dto
{
    public class BrandDto
    {
        public string Id
        {
            get;
            set;
        }

        public string Brand
        {
            get;
            set;
        }

        public BrandDto(string id, string brand)
        {
            Id = id;
            Brand = brand;
        }
    }
}