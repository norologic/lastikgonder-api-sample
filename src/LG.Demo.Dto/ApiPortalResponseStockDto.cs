﻿using Newtonsoft.Json;

namespace LG.Demo.Dto
{
    public class ApiPortalResponseStockDto
    {
        [JsonProperty("store")]
        public string Store
        {
            get;
            set;
        }

        [JsonProperty("amount")]
        public string Amount
        {
            get;
            set;
        }
    }
}